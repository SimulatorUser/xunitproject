using System;
using Trialproject;
using Xunit;

namespace XUnitTestProject2
{
    
        public class Class1
        {

            [Fact]
            public void PassingTest()
            {
                Assert.Equal(4, Add(2, 2));
            }

            [Fact]
            public void FailingTest()
            {
                Assert.Equal(5, Add(2, 2));
            }

            int Add(int x, int y)
            {
                return x + y;
            }

            [Fact]
            public void EmployeeTest()
            {
            Employee emp = new Employee();
            var expected = 1001;
            var actual = emp.EmpId;
            Assert.Equal(expected, actual);

         

            }
        }
    
}
